package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();
    VideoGame videoGame = new VideoGame();

    private HashMap<Boolean, VideoGame> videoGamesByStatus = new HashMap<>();
    VideoGame videoGameStatus = new VideoGame();


    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public VideoGame get(Long id) {
        return Optional.ofNullable(this.videoGamesById.get(id)).orElse(null);
    }

    public void save(VideoGame v) {
        this.videoGamesById.put(v.getId(), v);
    }

    public void status(VideoGame v) {
        this.videoGamesByStatus.put(videoGameStatus.getFinished(), videoGame);
    }

    public VideoGame filtreParGenre (){
        return Optional.ofNullable(videoGame).orElse(null);
    }

    public void addAGameToTheBase (Long id){
        videoGamesById.put(id,videoGame);

    }

    public void deleteGame(Long id) {
        videoGamesById.remove(id,videoGame);
    }


}
